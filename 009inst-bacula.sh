#!/bin/bash 
#Primero debemos asegurarnos de actualizar
apt-get update -y
#Luego instalamos los programas necesarios para bacula, apache2, mysql y php, es posible tener que revisar las versiones en futuras instalaciones
#Durante la instalacion de mysql se solicita una contraseña root de base de datos, recomiendo usar la misma que se emplee en bacula
apt-get install apache2 mysql-server-5.7 mysql-client-5.7 php7.0 -y
#Procedemos a la instalacion misma de bacula, la cual no presente mayor dificultad, que en un cierto momento se solicita la contraseña que desea para
# la base de datos que el mismo programa creara
apt-get install bacula bacula-client bacula-common-mysql bacula-director-mysql bacula-sd-mysql bacula-server -y

#Necesitamos para configurar el bacula, la direccion ip del servidor
#obtengo la informacion
ifconfig >> /home/bacula/IPinfo
#me quedo unicamente con la ip, y no con todo lo que resulta del ifconfig
ipser=$(grep 'inet addr:' /home/bacula/IPinfo | awk '{print $2}' | head -n 1 | sed 's/inet://g')
if [$ipser = ""]; then
                     #si ipser esta vacio es debido a que la distro estaba en ingles, por lo que el procedimiento anterior cambia un poco
                     ipser=$(grep 'inet' /home/bacula/IPinfo | awk '{print $2}' | head -n 1 | sed 's/inet://g')
                     fi
#Proceso para solicitar contraseña del bacula
bandera=true
while $bandera
do
echo "INGRESE LA CONTRASEÑA DE BACULA (SE RECOMIENDA LA MISMA QUE LA USADA PARA LA BASE DE DATOS):"
read contrasenia
echo "REINGRESE LA CONTRASEÑA PARA CONFIRMAR"
read contra2
if [ $contra2 = $contrasenia ]
 then
   bandera=false
 else
   echo "CONTRASEÑAS NO COINCIDEN"
 fi

done
#contrasenia="laquequiera"
#Primero modificamos el archivo bacula-dir.conf
#En el cual deberemos indicarle al director, cliente, storage y console la contrasenia elegida
#Tambien la ip del servidor
#La direccion donde se recuperaran los backups cuando sea necesario
sed -i "s/DirAddress = .*/DirAddress = ${ipser}/g" /etc/bacula/bacula-dir.conf
sed -i 's/DB Address/DB Address1/g' /etc/bacula/bacula-dir.conf
sed -i "s/Address = .*/Address = ${ipser} /g" /etc/bacula/bacula-dir.conf
sed -i "s/Password = \".*\"/Password = \"${contrasenia}\" /g" /etc/bacula/bacula-dir.conf
sed -i 's/DB Address1/DB Address/g' /etc/bacula/bacula-dir.conf
sed -i 's/Where = \/.*/Where = \/respaldos /g' /etc/bacula/bacula-dir.conf

#En el archivo bacula-sd.conf el procedimiento es similar, pero aqui se indica el lugar donde se guardaran los volumnes de backup
sed -i "s/SDAddress = .*/SDAddress = ${ipser} /g" /etc/bacula/bacula-sd.conf
sed -i "s/Password = \".*\"/Password = \"${contrasenia}\" /g" /etc/bacula/bacula-sd.conf
sed -i 's/Archive Device = \/.*/Archive Device = \/respaldos /g' /etc/bacula/bacula-sd.conf
#Es importante crear la carpeta donde se almacenara todo y darle los permisos
cd /
mkdir respaldos
chmod 777 respaldos/
#Modificamos el bacula-fd.conf y bconsole.conf de forma similar
sed -i "s/FDAddress = .*/FDAddress = ${ipser} /g" /etc/bacula/bacula-fd.conf
sed -i "s/Password = \".*\"/Password = \"${contrasenia}\" /g" /etc/bacula/bacula-fd.conf
sed -i "s/Password = \".*\"/Password = \"${contrasenia}\" /g" /etc/bacula/bconsole.conf
sed -i "s/address = .*/address = ${ipser} /g" /etc/bacula/bconsole.conf

#Por ultimo modificamos la base para que tome el nombre que corresponde
sed -i 's/XXX_DBNAME_XXX/bacula/g' /usr/share/bacula-director/make_mysql_tables

#Reiniciamos los servicios
/etc/init.d/mysql restart
/etc/init.d/bacula-director restart
/etc/init.d/bacula-sd restart
/etc/init.d/bacula-fd restart
######para el cliente
#Desgarmos el webmin, una interfaz que nos permitira manejar de forma sencilla los backups y las restauraciones
wget https://sourceforge.net/projects/webadmin/files/webmin/1.881/webmin_1.881_all.deb
dpkg -i webmin_1.881_all.deb
#Muy probablemente exitas problemas de dependencia que debmos solucionar con:
apt-get install -f -y
#Luego instalamos
dpkg -i webmin_1.881_all.deb
#Y el webmin necesita las librerias de mysql en perl, para mostrarnos el sistema bacula, por lo que lo instalamos
apt-get install libdbd-mysql-perl -y
#Reiniciamos el servicio de webmin
/etc/init.d/webmin restart
